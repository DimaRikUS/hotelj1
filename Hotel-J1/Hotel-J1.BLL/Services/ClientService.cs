﻿using Hotel_J1.DAL.DataModels;
using Hotel_J1.DAL.Repositories;

namespace Hotel_J1.BLL.Services
{
    public class ClientService
    {
        public void RegisterClients(string name, string surname, string patronymic, string phone, RoomModel reservedRoom)
        {
            var cr = new XmlClientRepository();
            var client = new ClientModel();
            client.ID = cr.getId() + 1;
            client.RoomID = reservedRoom.ID;
            client.Name = name;
            client.Surname = surname;
            client.Patronymic = patronymic;
            client.Phone = phone;

            cr.Add(client);

            new RoomService().ReserveRoom(reservedRoom.ID, reservedRoom.StartDateReserv, reservedRoom.EndDateReserv);
        }
    }
}
