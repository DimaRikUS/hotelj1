﻿using Hotel_J1.DAL.DataModels;
using Hotel_J1.DAL.Repositories;
using System.Linq;

namespace Hotel_J1.BLL.Services
{
    public sealed class RoomTypeService
    {       
        public void AddRoomType(string type, int capacity, double price)
        {
            var rtr = new XmlRoomTypeRepository();
            var roomType = new RoomTypeModel();

            roomType.ID = rtr.getId() + 1;
            roomType.Name = type;
            roomType.Capacity = capacity;
            roomType.Price = price;

            rtr.Add(roomType);
        }       

        public void UpdateRoomType(string type, int capacity, double price)
        {
            var rtr = new XmlRoomTypeRepository();
            var roomType = new RoomTypeModel();

            roomType = GetRoomType(type);
            roomType.Capacity = capacity;
            roomType.Price = price;

            rtr.Update(roomType);
        }

        public void DeleteRoomType(string type)
        {
            var rtr = new XmlRoomTypeRepository();
            var roomType = new RoomTypeModel();

            roomType = GetRoomType(type);

            rtr.Delete(roomType.ID);
        }

        public RoomTypeModel GetRoomType(string type)
        {
            var rtr = new XmlRoomTypeRepository();
            var roomType = new RoomTypeModel();
            var roomTypes = rtr.GetAll().ToList();

            roomType = roomTypes.Find(rs => rs.Name.ToLower().Equals(type.ToLower()));

            if (roomType == null)
            {
                roomType.ID = rtr.getId() + 1;
                roomType.Name = type;

                rtr.Add(roomType);
            }

            return roomType;
        }
    }
}
