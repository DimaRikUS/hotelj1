﻿using Hotel_J1.DAL.DataModels;
using Hotel_J1.BLL.ViewModels;
using Hotel_J1.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Hotel_J1.BLL.Services
{
    public sealed class RoomService
    {

        public void Delete(int id)
        {
            var rr = new XmlRoomRepository();
            var room = rr.GetById(id);

            rr.Delete(room.ID);
        }

        public void Edit(int id, string type, string status, DateTime startDate, DateTime endDate)
        {
            var roomType = GetRoomType(type);

            var roomStatus = GetRoomStatus(status);

            var rr = new XmlRoomRepository();
            var room = rr.GetById(id);

            room.StartDateReserv = startDate;
            room.EndDateReserv = endDate;
            room.RoomTypeID = roomType.ID;
            room.StatusID = roomStatus.ID;

            rr.Update(room);
        }

        public RoomStatusModel GetRoomStatus(string status)
        {
            var rsr = new XmlRoomStatusRepository();
            var roomStatuses = rsr.GetAll().ToList();
            var roomStatus = roomStatuses.Find(rs => rs.StatusName.ToLower().Equals(status.ToLower()));

            if (roomStatus == null)
            {
                roomStatus.ID = rsr.getId() + 1;
                roomStatus.StatusName = status;

                rsr.Add(roomStatus);
            }

            return roomStatus;
        }

        public void AddRoom(string type, string status)
        {
            var roomType = GetRoomType(type);

            var roomStatus = GetRoomStatus(status);

            var rr = new XmlRoomRepository();
            var room = new RoomModel();

            room.ID = rr.getId() + 1;
            room.RoomTypeID = roomType.ID;
            room.StatusID = roomStatus.ID;

            rr.Add(room);
        }

        public RoomTypeModel GetRoomType(string type)
        {
            var rtr = new XmlRoomTypeRepository();
            var roomTypes = rtr.GetAll().ToList();
            var roomType = roomTypes.Find(rs => rs.Name.ToLower().Equals(type.ToLower()));

            if (roomType == null)
            {
                roomType.ID = rtr.getId() + 1;
                roomType.Name = type;

                rtr.Add(roomType);
            }

            return roomType;
        }
        public RoomModel ReserveRoom(int id, DateTime start, DateTime end)
        {
            var rr = new XmlRoomRepository();
            var room = rr.GetById(id);

            room.ID = id;
            room.StatusID = 1;
            room.StartDateReserv = start;
            start.ToShortDateString();
            room.EndDateReserv = end;
            end.ToShortDateString();
            rr.Update(room);

            return room;
        }
        public IEnumerable<RoomViewModel> GetRoomsTable()
        {
            var rr = new XmlRoomRepository();
            var roomList = rr.GetAll();
            var rtr = new XmlRoomTypeRepository();

            return
                from room in roomList
                select new RoomViewModel
                {
                    ID = room.ID,
                    RoomTypeID = room.RoomTypeID,
                    RoomType = rtr.GetById(room.RoomTypeID).Name,
                    StatusID = room.StatusID,
                    Status = new XmlRoomStatusRepository().GetById(room.StatusID).StatusName,
                    StartDateReserv = room.StartDateReserv.Date,

                    EndDateReserv = room.EndDateReserv.Date,
                    Capasity = rtr.GetById(room.RoomTypeID).Capacity,
                    Price = rtr.GetById(room.RoomTypeID).Price
                };
        }
        public IEnumerable<RoomViewModel> GetReserveRoomsAtPeriod(DateTime start, DateTime end)
        {
            var rsr = new XmlRoomTypeRepository();
            var reserve = rsr.GetById(1);
            var rooms = GetRoomsTable();

            return
                from room in rooms
                orderby room.ID
                where room.StartDateReserv >= start && room.StartDateReserv <= end
                where room.StatusID == reserve.ID
                select new RoomViewModel
                {
                    ID = room.ID,
                    RoomTypeID = room.RoomTypeID,
                    RoomType = rsr.GetById(room.RoomTypeID).Name,
                    StatusID = room.StatusID,
                    Status = new XmlRoomStatusRepository().GetById(room.StatusID).StatusName,
                    StartDateReserv = room.StartDateReserv.Date,

                    EndDateReserv = room.EndDateReserv.Date,
                    Capasity = rsr.GetById(room.RoomTypeID).Capacity,
                    Price = rsr.GetById(room.RoomTypeID).Price
                };
        }
        public IEnumerable<RoomViewModel> SearchType(string type)
        {
            var rsr = new XmlRoomTypeRepository();
            var reserve = rsr.GetById(1);
            var rooms = GetRoomsTable();

            return
                from room in rooms
                orderby room.RoomType
                where room.RoomType == type
                select new RoomViewModel
                {
                    ID = room.ID,
                    RoomTypeID = room.RoomTypeID,
                    RoomType = new XmlRoomTypeRepository().GetById(room.RoomTypeID).Name,
                    StatusID = room.StatusID,
                    Status = new XmlRoomStatusRepository().GetById(room.StatusID).StatusName,
                    StartDateReserv = room.StartDateReserv.Date,

                    EndDateReserv = room.EndDateReserv.Date,
                    Capasity = rsr.GetById(room.RoomTypeID).Capacity,
                    Price = rsr.GetById(room.RoomTypeID).Price
                };
        }
    }
}
