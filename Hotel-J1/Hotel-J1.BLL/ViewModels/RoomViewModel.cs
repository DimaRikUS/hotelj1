﻿using Hotel_J1.DAL.DataModels;

namespace Hotel_J1.BLL.ViewModels
{
    public sealed class RoomViewModel : RoomModel
    {
        public string RoomType { get; set; }
        public string Status { get; set; }
        public int Capasity { get; set; }
        public double Price { get; set; }
    }
}
