﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hotel_J1.BLL.Services;
using Hotel_J1.Manager;
using Hotel_J1.BLL.ViewModels;

namespace Hotel_J1.Manager.UserRoom
{
    public class SearchRoomMethod
    {

        public bool SearchType()
        {
            Console.Clear();
            TextColor.SetYellowColor();
            Console.WriteLine("Enter the type of room (classic, president, lux): ");
            TextColor.SetBlackColor();
            try
            {
                var typeRoom = Console.ReadLine();
                var rs = new RoomService();
                var rooms = rs.SearchType(typeRoom);
                Console.Clear();
                PrintRoomTable(rooms);
            }
            catch
            {
                Exeptions.inputExeption();
                return false;
            }

            return true;
        }
        public void PrintRoomTable(IEnumerable<RoomViewModel> rooms)
        {
            Console.Clear();
            TextColor.SetGreenColor();
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("|Number|  Status  |   Type   |   Date   |End booked date|");
            Console.WriteLine("---------------------------------------------------------");

            foreach (var item in rooms)
            {
                var output = String.Format("|{0,-6}|{1,-10}|{2,-10}|{3,-10}|   {4,-10}  |", item.ID, item.Status, item.RoomType, item.StartDateReserv.ToShortDateString(), item.EndDateReserv.ToShortDateString());
                Console.WriteLine(output);
            }

            Console.WriteLine("---------------------------------------------------------");
        }
        public bool ReservedRoomsAtPeriod()
        {
            try
            {

                TextColor.SetYellowColor();
                Console.WriteLine("Enter the start date in format dd.mm.yyyy: ");
                TextColor.SetBlackColor();
                var startDate = DateTime.Parse(Console.ReadLine());

                TextColor.SetYellowColor();
                Console.WriteLine("Enter the end date in format dd.mm.yyyy: ");
                TextColor.SetBlackColor();
                var endDate = DateTime.Parse(Console.ReadLine());

                TextColor.SetGreenColor();
                var rs = new RoomService();
                var rooms = rs.GetReserveRoomsAtPeriod(startDate, endDate);

                PrintRoomTable(rooms);
            }
            catch
            {
                Exeptions.dateExeption();
                return false;
            }

            return true;
        }
    }
}
