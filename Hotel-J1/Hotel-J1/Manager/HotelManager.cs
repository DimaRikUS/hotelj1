﻿using Hotel_J1.BLL.Services;
using Hotel_J1.BLL.ViewModels;
using Hotel_J1.DAL.DataModels;
using Hotel_J1.Manager.UserRoom;
using Hotel_J1.Manager.AdminRoom;
using Hotel_J1.Manager;
using System;
using System.Collections.Generic;

namespace Hotel_J1
{
    public class HotelManager
    {
        public delegate bool ExecuteMethod();
        AdminOperation admin = new AdminOperation();
        SearchRoomMethod search = new SearchRoomMethod();

        public HotelManager()
        {
            Console.WindowHeight = Console.LargestWindowHeight;
            Console.WindowWidth = Console.LargestWindowWidth;
            Display();
            Output(SetAccount);

            Console.ReadKey();
        }
        public void Display()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("/================================================================================================================================================\\");
            Console.WriteLine("||                                                                                                                                              ||");
            Console.WriteLine("||                                                    Welcome to the reservations service                                                       ||");
            Console.WriteLine("||                                                                                                                                              ||");
            Console.WriteLine("||----------------------------------------------------------------------------------------------------------------------------------------------||");
            Console.WriteLine("||             The project Hotel_J1 - it is informative and easy to use service. Here you can quickly book a room at an affordable              ||");
            Console.WriteLine("||                                            price, taking into account different services.                                                    ||");
            Console.WriteLine("||----------------------------------------------------------------------------------------------------------------------------------------------||");
            Console.WriteLine("||       Full details of the rooms       |                             Saving                           |               Ease of use             ||");
            Console.WriteLine("||----------------------------------------------------------------------------------------------------------------------------------------------||");
            Console.WriteLine("|| For customer convenience, provides    | Book hotels by using Hotel_J1 provides you with              | Site menu allows you to get useful    ||");
            Console.WriteLine("|| basic information on numbers: their   | the best price without any loss of comfort and quality       | additional information. For example,  ||");
            Console.WriteLine("|| pricing policy, the interior,         | of life. The system eliminates the presence of               | you can see which rooms are available ||");
            Console.WriteLine("|| especially recreation,hotel services. | intermediaries, also we do not charge fees to our customers. | at the moment or in a  given period.  ||");
            Console.WriteLine("\\================================================================================================================================================/");
            Console.WriteLine();
        }
        public void Output(ExecuteMethod method)
        {
            do
            {
                method();
            } while (method());
        }

        public bool SetAccount()
        {
            TextColor.SetYellowColor();
            Console.WriteLine("Choose mode: 1 - admin; 2 - user:");
            TextColor.SetBlackColor();
            try
            {
                int _action = Int16.Parse(Console.ReadLine());
                switch (_action)
                {
                    case 1:
                        SetAdminAction();
                        break;
                    case 2:
                        SetUserRoom();
                        BookedRoom();
                        break;
                    default:
                        Exeptions.inputExeption();
                        break;
                }
            }
            catch
            {
                Exeptions.inputTypeExeption();
                return false;
            }

            return true;
        }

        public int GetNumber()
        {
            
            var id = 0;
            while(true)
            {
                try
                {
                    TextColor.SetYellowColor();
                    Console.WriteLine("Please entering number room: ");
                    TextColor.SetBlackColor();
                    id = Int16.Parse(Console.ReadLine());

                   
                }
                catch
                {
                    Exeptions.inputExeption();
                   
                };
                return id;
            };

           
        }

        public bool SetAdminAction()
        {
            Console.Clear();
            TextColor.SetYellowColor();
            Console.WriteLine("Choose action: 1 - add room; 2 - edit room; 3 - delete room; 4- back to start menu");
            TextColor.SetBlackColor();
            var rs = new RoomService();
            var rooms = rs.GetRoomsTable();

            try
            {
                int _action = Int16.Parse(Console.ReadLine());
                switch (_action)
                {
                    case 1:
                        search.PrintRoomTable(rooms);
                        admin.AddRoom();
                        break;

                    case 2:
                        Console.Clear();
                        search.PrintRoomTable(rooms);
                        var editedId = GetNumber();
                        admin.EditRoom(editedId);
                        break;
                    case 3:
                        Console.Clear();
                        search.PrintRoomTable(rooms);
                        var deletedId = GetNumber();
                        admin.DeleteRoom(deletedId);

                        break;
                    case 4:
                        Console.Clear();
                        SetAccount(); break;
                    default:
                        Exeptions.inputExeption();
                        break;
                }
            }
            catch
            {
                Console.Clear();
                Exeptions.inputTypeExeption();
                return false;
            }
            SetMenu();
            return true;
        }

        public bool ReserveRoom()
        {
            try
            {
                TextColor.SetYellowColor();
                Console.WriteLine("Please entering number room: ");
                TextColor.SetBlackColor();
                var _id = Int16.Parse(Console.ReadLine());
                TextColor.SetYellowColor();
                Console.WriteLine("Please entering start of reservation room in format dd.mm.yyyy: ");
                TextColor.SetBlackColor();
                var _start = DateTime.Parse(Console.ReadLine());

                TextColor.SetYellowColor();
                Console.WriteLine("Please entering end of reservation room in format dd.mm.yyyy: ");
                TextColor.SetBlackColor();
                var _end = DateTime.Parse(Console.ReadLine());

                var rr = new RoomService();
                var reservedRoom = rr.ReserveRoom(_id, _start, _end);

                Console.Clear();
                TextColor.SetYellowColor();
                Console.WriteLine("Please leave your personal data:\n");
                RegisterClientsInRoom(reservedRoom);

            }
            catch
            {
                Exeptions.inputExeption();
                return false;
            }
            SetContinueAction();
            return true;
        }
        public bool RegisterClientsInRoom(RoomModel reservedRoom)
        {
            try
            {
                TextColor.SetYellowColor();
                Console.WriteLine("Enter your name: ");
                TextColor.SetBlackColor();
                var name = Console.ReadLine();

                TextColor.SetYellowColor();
                Console.WriteLine("Enter your surname: ");
                TextColor.SetBlackColor();
                var surname = Console.ReadLine();

                TextColor.SetYellowColor();
                Console.WriteLine("Enter your patrinymic: ");
                TextColor.SetBlackColor();
                var patronymic = Console.ReadLine();

                TextColor.SetYellowColor();
                Console.WriteLine("Enter your phone: ");
                TextColor.SetBlackColor();
                var phone = Console.ReadLine();

                var rc = new ClientService();
                rc.RegisterClients(name, surname, patronymic, phone, reservedRoom);

                Console.Clear();
                TextColor.SetGreenColor();
                Console.WriteLine(".------------------------------------.");
                Console.WriteLine("| The room is reserved successfully! |");
                Console.WriteLine("'------------------------------------'");
                Console.ReadLine();

            }
            catch
            {
                Exeptions.inputTypeExeption();
                return false;
            }

            return true;
        }
        public bool SetContinueAction()
        {
            TextColor.SetYellowColor();
            Console.WriteLine("Do you want to continue working in the program? (1 - yes; 2 - no)");
            try
            {
                int _action = Int16.Parse(Console.ReadLine());
                switch (_action)
                {
                    case 1:
                        Console.Clear();
                        SetMenu();
                        break;
                    case 2:
                        Environment.Exit(0);
                        break;
                    default:
                        Exeptions.inputExeption();
                        break;
                }
            }
            catch
            {
                Console.Clear();
                Exeptions.inputTypeExeption();
                return false;
            }
            return true;

        }

        public void SetMenu()
        {
            TextColor.SetYellowColor();
            Console.WriteLine("Please choose menu: 1 - start menu, 2 - admin menu");
            TextColor.SetBlackColor();
            var values = Int16.Parse(Console.ReadLine());
            switch (values)
            {
                case 1: SetAccount(); break;
                case 2: SetAdminAction(); break;
                default:
                    Exeptions.inputExeption();
                    break;
            }

        }

        public bool SetUserRoom()
        {
            Console.Clear();
            TextColor.SetYellowColor();
            Console.WriteLine("What you want to do? (1 - view all rooms in hotel; 2 - search room by propreties; 3 - return to the initial menu; 4 - exit):");
            TextColor.SetBlackColor();
            var rs = new RoomService();
            var rooms = rs.GetRoomsTable();
            try
            {
                int _action = Int16.Parse(Console.ReadLine());
                switch (_action)
                {
                    case 1:

                        DisplayDetails();
                        search.PrintRoomTable(rooms);

                        break;
                    case 2:
                        Console.Clear();
                        SearchRoomByProp();
                        break;
                    case 3:
                        Console.Clear();
                        SetMenu();
                        break;
                    case 4:
                        Environment.Exit(0);
                        break;
                    default:
                        Exeptions.inputExeption();
                        break;
                }
            }
            catch
            {
                Exeptions.inputTypeExeption();
                return false;
            }
            BookedRoom();
            SetContinueAction();
            return true;
        }

        public bool SearchRoomByProp()
        {
            Console.Clear();
            TextColor.SetYellowColor();
            Console.WriteLine("Select search options (1 - search by date; 2 - search by type):");
            TextColor.SetBlackColor();
            try
            {
                int _action = Int16.Parse(Console.ReadLine());
                switch (_action)
                {
                    case 1:
                        Console.Clear();
                        search.ReservedRoomsAtPeriod();
                        break;
                    case 2:
                        Console.Clear();
                        search.SearchType();
                        break;
                    default:
                        Exeptions.inputExeption();
                        break;
                }
            }
            catch
            {
                Exeptions.inputTypeExeption();
                return false;
            }

            return true;
        }

        public void DisplayDetails()
        {
            Console.Clear();
            TextColor.SetYellowColor();
            Console.WriteLine("Rooms at the hotel: ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            var rs = new RoomService();
            var rooms = rs.GetRoomsTable();

            foreach (var p in rooms)
            {
                switch (p.Status)
                {
                    case "free":
                        TextColor.SetGreenColor();
                        Console.WriteLine("Room: {0}:", p.ID);
                        PrintRoom(p);
                        TextColor.SetGreenColor();
                        Console.WriteLine(p.Status);
                        TextColor.SetBlackColor();
                        break;
                    case "reserve":
                        TextColor.SetMagentaColor();
                        Console.WriteLine("Room: {0}:", p.ID);
                        PrintRoom(p);
                        TextColor.SetMagentaColor();
                        Console.WriteLine(p.Status); //status is here:)
                        TextColor.SetBlackColor();
                        break;
                    default:
                        break;
                }
            }
            Console.WriteLine("Please, press Enter to go to the reservation number!");
            Console.ReadKey();
        }

        public void PrintRoom(RoomViewModel p)
        {
            TextColor.SetBlackColor();
            Console.Write("\n\tClass Room: ");
            TextColor.SetBlueColor();
            Console.WriteLine(p.RoomType);

            TextColor.SetBlackColor();
            Console.Write("\tCapacity Room: ");
            TextColor.SetBlueColor();
            Console.WriteLine(p.Capasity);

            TextColor.SetBlackColor();
            Console.Write("\tCheck: ");
            TextColor.SetBlueColor();
            Console.WriteLine(p.StartDateReserv);

            TextColor.SetBlackColor();
            Console.Write("\tCheckOut: ");
            TextColor.SetBlueColor();
            Console.WriteLine(p.EndDateReserv);

            TextColor.SetBlackColor();
            Console.Write("\tCosts: : ");
            TextColor.SetBlueColor();
            Console.WriteLine("${0}", p.Price);

            TextColor.SetBlackColor();
            Console.Write("\tStatus: ");
        }

        public bool BookedRoom()
        {
            TextColor.SetYellowColor();
            Console.WriteLine("Choose action: 1 - booked, 2 - back to user menu, 0 - exit");
            TextColor.SetBlackColor();
            try
            {
                int _action = Int16.Parse(Console.ReadLine());
                switch (_action)
                {
                    case 1:
                        Output(ReserveRoom);
                        DisplayDetails();
                        break;
                    case 2:
                        SetUserRoom();
                        break;
                    case 0:
                        Environment.Exit(0);
                        break;

                    default:
                        Exeptions.inputExeption();
                        break;
                }
            }
            catch
            {
                Exeptions.inputTypeExeption();
                return false;
            }
            SetContinueAction();
            return true;
        }
    }
}
