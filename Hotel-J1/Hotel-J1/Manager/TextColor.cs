﻿using System;

namespace Hotel_J1.Manager
{
    public static class TextColor
    {
        public static void SetYellowColor()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
        }
        public static void SetBlackColor()
        {
            Console.ForegroundColor = ConsoleColor.White;
        }
        public static void SetBlueColor()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
        }
        public static void SetRedColor()
        {
            Console.ForegroundColor = ConsoleColor.Red;
        }
        public static void SetWhiteColor()
        {
            Console.ForegroundColor = ConsoleColor.White;
        }
        public static void SetCyanColor()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
        }
        public static void SetGreenColor()
        {
            Console.ForegroundColor = ConsoleColor.Green;
        }
        public static void SetMagentaColor()
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
        }
    }
}
