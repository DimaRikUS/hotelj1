﻿using Hotel_J1.BLL.Services;
using System;

namespace Hotel_J1.Manager.AdminRoom
{
    public class AdminOperation
    {

        public void AddRoom()
        {
            try
            {
                TextColor.SetYellowColor();
                Console.Write("Please enter room type: ");
                TextColor.SetBlackColor();
                var roomType = Console.ReadLine();

                TextColor.SetYellowColor();
                Console.Write("Please enter room status: ");
                TextColor.SetBlackColor();
                var status = Console.ReadLine();

                TextColor.SetBlackColor();
                var rs = new RoomService();
                TextColor.SetGreenColor();
                Console.WriteLine(".--------------------.");
                Console.WriteLine("| Add room complete! |");
                Console.WriteLine("'--------------------'");
                rs.AddRoom(roomType, status);
            }
            catch
            {
                Exeptions.inputTypeExeption();
            }
        }

        public void DeleteRoom(int _id)
        {
            var rs = new RoomService();
            TextColor.SetGreenColor();
            Console.WriteLine(".--------------.");
            Console.WriteLine("| Room delete! |");
            Console.WriteLine("'--------------'");
            rs.Delete(_id);
        }

        public void EditRoom(int _id)
        {
            try
            {
                TextColor.SetYellowColor();
                Console.Write("Please enter room type: ");
                TextColor.SetBlackColor();
                var roomType = Console.ReadLine();

                TextColor.SetYellowColor();
                Console.Write("Please enter room status: ");
                TextColor.SetBlackColor();
                var status = Console.ReadLine();
                var rs = new RoomService();

                TextColor.SetYellowColor();
                Console.WriteLine("Please entering start of reservation room: ");
                TextColor.SetBlackColor();
                var _start = DateTime.Parse(Console.ReadLine());

                TextColor.SetYellowColor();
                Console.WriteLine("Please entering end of reservation room: ");
                TextColor.SetBlackColor();
                var _end = DateTime.Parse(Console.ReadLine());
                TextColor.SetGreenColor();
                Console.WriteLine(".-----------------.");
                Console.WriteLine("| Edit room save! |");
                Console.WriteLine("'-----------------'");
                rs.Edit(_id, roomType, status, _start, _end);
            }
            catch
            {
                Exeptions.inputTypeExeption();
            }

        }
    }
}
