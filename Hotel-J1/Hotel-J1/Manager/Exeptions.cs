﻿using System;

namespace Hotel_J1.Manager
{
    public class Exeptions
    {
        public static void inputExeption()
        {
            TextColor.SetRedColor();
            Console.WriteLine("Please, follow the instructions and try again!");
            TextColor.SetBlackColor();
        }

        public static void inputTypeExeption()
        {
            TextColor.SetRedColor();
            Console.WriteLine("Invalid input!");
            TextColor.SetBlackColor();
        }

        public static void dateExeption()
        {
            TextColor.SetRedColor();
            Console.WriteLine("Please, enter the date in this (dd.mm.yyyy) format!");
            TextColor.SetBlackColor();
        }
    }
}
