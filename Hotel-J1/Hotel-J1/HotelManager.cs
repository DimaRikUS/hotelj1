﻿using System;
using Hotel_J1.BLL;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hotel_J1.BLL.HotelRoom;
using Hotel_J1.BLL.Clients;
using Hotel_J1.BLL.Operation;

namespace Hotel_J1
{
    public class HotelManager
    {
        Reservations _resrv = new Reservations();
        Searchs _s = new Searchs();
        SearchType _t = new SearchType();
        private List<Room> Rooms;

        public HotelManager()
        {
            var manager = new RoomRepo();
            Rooms = manager.initialList();
            Display();
            Choosing();
        }

        public void Display()
        {
            Console.BackgroundColor = ConsoleColor.White;
            SetYellowColor();
            Console.WindowHeight = Console.LargestWindowHeight;
            Console.WindowWidth = Console.LargestWindowWidth;
            Console.WriteLine("  Rooms at the hotel: ");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Cyan;
            foreach (Room p in Rooms)
            {
                switch (p.Status)
                {
                    case "Free":
                        SetGreenColor();
                        Console.WriteLine("  Room: {0}:", p.IdRoom);
                        PrintRoom(p);
                        SetGreenColor();
                        Console.WriteLine(p.Status);
                        SetBlackColor();
                        break;
                    case "Busy":
                        SetRedColor();
                        Console.WriteLine("  Room: {0}:", p.IdRoom);
                        PrintRoom(p);
                        SetRedColor();
                        Console.WriteLine(p.Status);
                        SetBlackColor();
                        break;
                    case "Booked":
                        SetMagentaColor();
                        Console.WriteLine("  Room: {0}:", p.IdRoom);
                        PrintRoom(p);
                        SetMagentaColor();
                        Console.WriteLine(p.Status);
                        SetBlackColor();
                        break;
                    default:
                        break;
                }
                Console.WriteLine();
            }



        }

        private static void PrintRoom(Room p)
        {
            SetBlackColor();
            Console.Write("\n\tClass Room: ");
            SetBlueColor();
            Console.WriteLine(p.TypeHotelRoom);
            SetBlackColor();
            Console.Write("\tCapacity Room: ");
            SetBlueColor();
            Console.WriteLine(p.CapasityRoom);
            SetBlackColor();
            Console.Write("\tCheck: ");
            SetBlueColor();
            Console.WriteLine(p.Check);
            SetBlackColor();
            Console.Write("\tCheckOut: ");
            SetBlueColor();
            Console.WriteLine(p.CheckOut);
            SetBlackColor();
            Console.Write("\tCosts: : ");
            SetBlueColor();
            Console.WriteLine("${0}", p.Costs);
            SetBlackColor();
            Console.Write("\tStatus: ");
        }

        private static void SetYellowColor()
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
        }

        private static void SetBlackColor()
        {
            Console.ForegroundColor = ConsoleColor.Black;
        }

        private static void SetBlueColor()
        {
            Console.ForegroundColor = ConsoleColor.DarkBlue;
        }


        private static void SetRedColor()
        {
            Console.ForegroundColor = ConsoleColor.Red;
        }

        private static void SetWhiteColor()
        {
            Console.ForegroundColor = ConsoleColor.White;
        }
        private static void SetCyanColor()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
        }


        private static void SetGreenColor()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
        }

        private static void SetMagentaColor()
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
        }

        //public void Search()
        //{ 


        //            Console.WriteLine("Writre date: ");
        //            string data = Console.ReadLine();

        //            //DateTime date = DateTime.Parse(data);
        //            //var number = Rooms.Where(numbers => numbers.Check.ToString() == date.ToString());
        //            //if (number != null)
        //            //{
        //            var d = _s.Search();
        //                foreach (Room r in number)
        //                {
        //                    Console.Write("Room: {0}: \n\tClass Room: {1}; \n\tCapacity Room: {2}; \n\tCheck: {3}; \n\tCheckout: {4}; \n\tCosts: {5}; \n\tStatus: ", r.IdRoom, r.TypeHotelRoom, r.CapasityRoom, r.Check, r.CheckOut, r.Costs);
        //                    switch (r.Status)
        //                    {
        //                        case "Free":
        //                            Console.ForegroundColor = ConsoleColor.Green;
        //                            PrintStatus(r);
        //                            break;
        //                        case "Busy":
        //                            Console.ForegroundColor = ConsoleColor.Red;
        //                            PrintStatus(r);
        //                            break;
        //                        case "Booked":
        //                            Console.ForegroundColor = ConsoleColor.Magenta;
        //                            PrintStatus(r);
        //                            break;
        //                        default:
        //                            break;

        //                    };



        public void Choosing()
        {
            SetYellowColor();
            Console.WriteLine("  Choose action: 1 - booked, 0 - exit");
            SetWhiteColor();
            int _action = Int16.Parse(Console.ReadLine());
            switch (_action)
            {
                case 1:
                    {
                        SetYellowColor();
                        Console.WriteLine("Please entering number room: ");
                        SetBlackColor();
                        var _room = Int16.Parse(Console.ReadLine());
                        _resrv.Reservation(_room);
                    }; break;
                    
                case 0: return;
                default:
                    SetYellowColor();
                    Console.WriteLine("Please choose action!");
                    SetBlackColor();
                    break;

            }

        }
    }
}
