﻿using Hotel_J1.DAL.DataModels;
using System.Linq;

namespace Hotel_J1.DAL.Repositories
{
    public class XmlFakeRepository : XmlRepository<FakeModel>, IRepository<FakeModel>
    {  
        public void Delete(int id)
        {
            var model = GetById(id);
            _context.Remove(model);

            WriteToFile(_context);
        }

        public FakeModel GetById(int id)
        {
            var fake = _context.Find(m => m.ID == id);

            return fake;
        }

        public void Update(FakeModel model)
        {
            var temp = _context.Find(m => m.ID == model.ID);
            if (temp != null)
            {
                temp.ID = model.ID;
                temp.Name = model.Name;
            }
            else
            {
                _context.Add(model);
            }

            WriteToFile(_context);
        }

        public int getId()
        {
            int id = 0;

            if (_context.Count != 0)
            {
                var last = _context.AsEnumerable().LastOrDefault();
                id = last.ID;
            }

            return id;
        }
    }
}
