﻿using System.Collections.Generic;

namespace Hotel_J1.DAL.Repositories
{
    interface IRepository<TModel>
        where TModel : class
    {
        IEnumerable<TModel> GetAll();
        TModel GetById(int id);
        void Add(TModel model);
        void Update(TModel model);
        void Delete(int id);
    }
}
