﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Hotel_J1.DAL.Repositories
{
    public class XmlRepository<T>
        where T : class
    {
        protected List<T> _context;

        public XmlRepository()
        {
            _context = ReadFile();
        }

        public void Add(T model)
        {
            _context.Add(model);

            WriteToFile(_context);
        }
        public IEnumerable<T> GetAll()
        {
            return _context;
        }

        #region FileHandle

        protected string GetFileName()
        {
            var typeParameter = typeof(T);
            var names = typeParameter.ToString();
            string[] name = names.Split('.');

            return name[name.Length - 1];
        }
        protected string PathToFile()
        {
            var fileName = GetFileName();

            var path = @"..\..\..\Hotel-J1.DAL\App_Data\" + fileName + ".xml";

            return Path.GetFullPath(path);
        }

        protected void WriteToFile(List<T> context)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<T>));

            using (var fs = new FileStream(PathToFile(), FileMode.Create))
            {
                formatter.Serialize(fs, context);
            }
        }
        protected List<T> ReadFile()
        {
            var context = new List<T>();
            var path = PathToFile();

            if (!File.Exists(path))
            {
                WriteToFile(context);
            }

            XmlSerializer formatter = new XmlSerializer(typeof(List<T>));

            using (var fs = new FileStream(PathToFile(), FileMode.Open))
            {
                context = (List<T>)formatter.Deserialize(fs);

                return context;
            }
        }

        #endregion
    }
}
