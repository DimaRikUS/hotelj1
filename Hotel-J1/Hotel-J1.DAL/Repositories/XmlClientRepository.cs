﻿using Hotel_J1.DAL.DataModels;
using System.Linq;

namespace Hotel_J1.DAL.Repositories
{
    public sealed class XmlClientRepository : XmlRepository<ClientModel>, IRepository<ClientModel>
    {
        public void Delete(int id)
        {
            var model = GetById(id);
            _context.Remove(model);

            WriteToFile(_context);
        }
        public ClientModel GetById(int id)
        {
            var client = _context.Find(m => m.ID == id);

            return client;
        }
        public void Update(ClientModel model)
        {
            var temp = _context.Find(m => m.ID == model.ID);
            if (temp != null)
            {
                temp.ID = model.ID;
                temp.RoomID = model.RoomID;
                temp.Name = model.Name;
                temp.Surname = model.Surname;
                temp.Patronymic = model.Patronymic;
                temp.Phone = model.Phone;
            }
            else
            {
                _context.Add(model);
            }

            WriteToFile(_context);
        }

        public int getId()
        {
            int id = 0;

            if (_context.Count != 0)
            {
                var last = _context.AsEnumerable().LastOrDefault();
                id = last.ID;
            }

            return id;
        }
    }
}
