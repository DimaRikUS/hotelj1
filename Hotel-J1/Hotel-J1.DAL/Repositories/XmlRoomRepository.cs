﻿using Hotel_J1.DAL.DataModels;
using System.Linq;

namespace Hotel_J1.DAL.Repositories
{
    public sealed class XmlRoomRepository : XmlRepository<RoomModel>, IRepository<RoomModel>
    {
        public void Delete(int id)
        {
            var model = GetById(id);
            _context.Remove(model);

            WriteToFile(_context);
        }
        public RoomModel GetById(int id)
        {
            var client = _context.Find(m => m.ID == id);

            return client;
        }

        public void Update(RoomModel model)
        {
            var temp = _context.Find(m => m.ID == model.ID);
            if (temp != null)
            {
                temp.ID = model.ID;
                temp.RoomTypeID = model.RoomTypeID;
                temp.StatusID = model.StatusID;
                temp.StartDateReserv = model.StartDateReserv;
                temp.EndDateReserv = temp.EndDateReserv;
            }
            else
            {
                _context.Add(model);
            }

            WriteToFile(_context);
        }

        public int getId()
        {
            int id = 0;

            if (_context.Count != 0)
            {
                var last = _context.AsEnumerable().LastOrDefault();
                id = last.ID;
            }

            return id;
        }
    }
}
