﻿using Hotel_J1.DAL.DataModels;
using System.Linq;

namespace Hotel_J1.DAL.Repositories
{
    public sealed class XmlRoomStatusRepository : XmlRepository<RoomStatusModel>, IRepository<RoomStatusModel>
    {
        public void Delete(int id)
        {
            var model = GetById(id);
            _context.Remove(model);

            WriteToFile(_context);
        }
        public RoomStatusModel GetById(int id)
        {
            var client = _context.Find(m => m.ID == id);

            return client;
        }
        public void Update(RoomStatusModel model)
        {
            var temp = _context.Find(m => m.ID == model.ID);
            if (temp != null)
            {
                temp.ID = model.ID;
                temp.StatusName = model.StatusName;
            }
            else
            {
                _context.Add(model);
            }

            WriteToFile(_context);
        }

        public int getId()
        {
            int id = 0;

            if (_context.Count != 0)
            {
                var last = _context.AsEnumerable().LastOrDefault();
                id = last.ID;
            }

            return id;
        }
    }
}
