﻿using System;
using System.Xml.Serialization;

namespace Hotel_J1.DAL.DataModels
{
    [Serializable]
    public class RoomModel
    {
        [XmlAttribute]
        public int ID { get; set; }
        public int RoomTypeID { get; set; }
        public int StatusID { get; set; }
        public DateTime StartDateReserv { get; set; }
        public DateTime EndDateReserv { get; set; }        

        public RoomModel() { }
    }
}
