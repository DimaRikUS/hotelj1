﻿using System;
using System.Xml.Serialization;

namespace Hotel_J1.DAL.DataModels
{
    [Serializable]
    public class FakeModel
    {
        [XmlAttribute]
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
