﻿using System;
using System.Xml.Serialization;

namespace Hotel_J1.DAL.DataModels
{
    [Serializable]
    public class RoomTypeModel
    {
        [XmlAttribute]
        public int ID { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int Capacity { get; set; }

        public RoomTypeModel() { }
    }
}
