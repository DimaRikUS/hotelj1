﻿using System;
using System.Xml.Serialization;

namespace Hotel_J1.DAL.DataModels
{
    [Serializable]
    public class RoomStatusModel
    {
        [XmlAttribute]
        public int ID { get; set; }
        public string StatusName { get; set; }

        public RoomStatusModel() { }
    }
}
