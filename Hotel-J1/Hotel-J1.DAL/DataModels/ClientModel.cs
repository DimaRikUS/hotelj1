﻿using System;
using System.Xml.Serialization;

namespace Hotel_J1.DAL.DataModels
{
    [Serializable]
    public class ClientModel
    {
        [XmlAttribute]
        public int ID { get; set; }
        public int RoomID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string Phone { get; set; }

        public ClientModel() { }
    }
}
